#!/usr/bin/env node

var gatecoin = require('gatecoin');


var gc = new gatecoin.Gatecoin({
    apiHost:        process.env.GATECOIN_HOST,
    apiKey:         process.env.GATECOIN_KEY,
    apiSecret:      process.env.GATECOIN_SECRET,
});


console.log("Connecting to the exchange...");
gc.connect().then(function (exch) {

    console.log("Firing off some requests...");

    var pairs = ['BTCUSD', 'LTCEUR']; //, 'XXXYYY'];
    var pending = [];
    for (var i = 0; i < pairs.length; i++) {
        pending.push(exch.getQuote(pairs[i]).then(quote => console.log('%s: %s', quote.symbol, quote)));
    }
    pending.push(exch.getBalance('BTC').then(bal => console.log('BTC balance: %s', bal.balance)));

    return Promise.all(pending);

}).then(() => { console.log("all done!"); }).catch(err => { console.log("something went wrong: %s", err); });
