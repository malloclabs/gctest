# gctest

This is a very simple example project demonstrating the usage of the [gatecoin-client](https://bitbucket.org/malloclabs/gatecoin-client) library.

Note that this example is *not* written in TypeScript, so it isn't taking advantage of
the `.d.ts` definitions included as part of the `gatecoin-client` package.

## Usage

Put your API key+secret into enviornment variables and run the script:

    $ export GATECOIN_HOST=api.gatecoin.com
    $ export GATECOIN_KEY="your_api_key"
    $ export GATECOIN_SECRET="0123456789ABCDEF"
    $ bin/gctest.js
